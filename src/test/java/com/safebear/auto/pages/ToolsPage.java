package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

@RequiredArgsConstructor
public class ToolsPage {

    public ToolsPageLocators locators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    public void printListOfTestTools(){

        List<WebElement> tools = driver.findElements(locators.getTestRows());

        for (WebElement tool : tools) {

            if (tool.isDisplayed()){

                System.out.println("yay!");

            }

        }

    }
}

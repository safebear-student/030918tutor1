package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    private By testRows = By.xpath(".//td[contains(text(),'test') or contains(text(),'Test')]");

}

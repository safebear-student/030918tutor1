package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class LoginPageLocators {

    // title
    private String pageTitleLocator = "Login Page";

    // fields
    private By usernameFieldLocator = By.id("username");
    private By passwordFieldLocator = By.name("psw");

    // buttons
    private By loginButtonLocator = By.id("enter");

}

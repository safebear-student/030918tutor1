package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test
    public void loginTest(){

        // Step 1 ACTION: Open our Web App
        driver.get(Utils.getUrl());
        // Step 1 EXPECTED RESULT
        Assert.assertEquals(loginPage.getTitle(), "Login Page");

        // Step 2 ACTION: Login
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        // Step 2 EXPECTED RESULT: Now on the tools page

        Assert.assertEquals(tPage.getPageTitle(), "Tools Page", "The Tools Page failed to open, or the login was unsuccessful or the page title has changed");

        tPage.printListOfTestTools();

    }

}
